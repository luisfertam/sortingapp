package com.luisfertam;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class AppTest {

    @Parameterized.Parameters(name = "{index}: sort({0}) = {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { new int[] { 1, 3, 2 }, new int[] { 1, 2, 3 } },
                { new int[] { 5, 1, 4, 2, 8 }, new int[] { 1, 2, 4, 5, 8 } },
                // Add more test cases for corner cases
                { new int[] {}, new int[] {} }, // Empty array
                { new int[] { 5 }, new int[] { 5 } }, // Single element
                { new int[] { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 }, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 } }, // More than 10 elements
        });
    }

    private int[] input;
    private int[] expectedOutput;

    public SortingAppTest(int[] input, int[] expectedOutput) {
        this.input = input;
        this.expectedOutput = expectedOutput;
    }

    @Test
    public void testSort() {
        SortingApp.sort(input);
        assertArrayEquals(expectedOutput, input);
    }
}