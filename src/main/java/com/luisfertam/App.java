package com.luisfertam;

import java.util.Arrays;

/**
 * A utility class for sorting arrays of integers.
 */
public class SortingApp {

    /**
     * Sorts the given array of integers in ascending order.
     *
     * @param arr the array of integers to be sorted
     */
    public static void sort(int[] arr) {
        Arrays.sort(arr);
    }
}
